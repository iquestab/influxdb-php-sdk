<?php
/**
 * Created by PhpStorm.
 * User: iquest
 * Date: 2017-09-08
 * Time: 11:36
 */

namespace InfluxDB\Adapter\Http;

interface OptionsInterface
{
    public function getPrefix();

    public function setPrefix($prefix);

    public function getTags();

    public function setTags($tags);

    public function getRetentionPolicy();

    public function setRetentionPolicy($retentionPolicy);

    public function getProtocol();

    public function setProtocol($protocol);

    public function getHost();

    public function setHost($host);

    public function getPort();

    public function setPort($port);

    public function getUsername();

    public function setUsername($username);

    public function getPassword();

    public function setPassword($password);

    public function getDatabase();

    public function setDatabase($database);
}