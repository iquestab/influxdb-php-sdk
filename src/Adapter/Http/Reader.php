<?php
namespace InfluxDB\Adapter\Http;

use GuzzleHttp\Client;
use InfluxDB\Adapter\QueryableInterface;

class Reader implements QueryableInterface
{
    private $httpClient;
    private $options;

    public function __construct(Client $httpClient, OptionsInterface $options)
    {
        $this->httpClient = $httpClient;
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function query($query)
    {
        $options = [
            "auth" => [$this->getOptions()->getUsername(), $this->getOptions()->getPassword()],
            'query' => [
                "q" => $query,
                "db" => $this->getOptions()->getDatabase(),
            ]
        ];

        return $this->post("query", $options);
    }
    
    public function admin($query)
    {
        $options = [
            "auth" => [$this->getOptions()->getUsername(), $this->getOptions()->getPassword()],
            'multipart' => [
                ["name" => "q", "contents" => $query],
                ["name" => "db", "contents" => $this->getOptions()->getDatabase()],
            ]
        ];

        return $this->post("admin", $options);
    }

    private function post($operation, array $httpMessage)
    {
        $endpoint = $this->getHttpEndpoint($operation);
        return json_decode($this->httpClient->post($endpoint, $httpMessage)->getBody(), true);
    }

    protected function getHttpQueryEndpoint()
    {
        return $this->getHttpEndpoint("query");
    }

    private function getHttpEndpoint($operation)
    {
        $url = sprintf(
            "%s://%s:%d%s/%s",
            $this->getOptions()->getProtocol(),
            $this->getOptions()->getHost(),
            $this->getOptions()->getPort(),
            $this->getOptions()->getPrefix(),
            $operation
        );

        return $url;
    }
}
