<?php

namespace InfluxDB\Adapter\Udp;

interface OptionsInterface
{
    public function getPort();

    public function setPort($port);

    public function getTags();

    public function setTags($tags);

    public function getHost();

    public function setHost($host);
}